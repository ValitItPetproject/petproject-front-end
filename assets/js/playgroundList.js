function generatePlaygroundList(playgroundList) {
    let rows = "";

    for (let i = 0; i < playgroundList.length; i++) {
        rows += `
        <div class="row">
            <div class="col-md-12>
            ${generatePlaygroundRowHeading(playgroundList[i])}
            </div>
        `;
    }
    document.getElementById("playgroundList").innerHTML = `
        <div class="row justify-content-center">
            <div class="col-lg-10">
        ${rows}
            </div>
        </div>
    </div>
    `;
}



function generatePlaygroundRowHeading(id) {
    return `
    <div class="accordion" id="accordionExample">
        <div class="card">
            <div class="card-header" style="width:700px" id="heading-${id.id}">
                <h2 class="mb-0">
            <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapse-${id.id}" aria-expanded="false" aria-controls="collapse-${id.id}">
                ${id.location + " / " + id.playground}
            </button>
                </h2>
            </div>
            <div id="collapse-${id.id}" class="collapse" aria-labelledby="heading-${id.id}" data-parent="#accordionExample">
                <div class="card-body">
                ${generatePlaygroundImage(id.playgroundPhoto)}
                </div>
            </div>
        </div>
    </div>
    `;
}

function generatePlaygroundRowTitleCell(text) {
    return `
        <div class="col-sm-6" style="padding: 5px;">
            <div style="background: #EAEDED; border-top: 1px solid #CCD1D1; border-bottom: 1px solid #CCD1D1; margin: 2px; padding: 10px; text-align: center;">
                <strong>${text}</strong>
            </div>
        </div>
    `;
}

function generatePlaygroundImage(playgroundPhoto) {
    if (!isEmpty(playgroundPhoto)) {
        return `<img src="${playgroundPhoto}" width="650"/>`;
    } else {
        return "[PILT PUUDUB]";
    }
}