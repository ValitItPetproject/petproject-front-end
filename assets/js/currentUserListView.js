function generateCurrentUserList(user) {
    let rows = "";
    let dogInfo = "";
    for (let i = 0; i < user.userdogs.length; i++) {
        dogInfo = dogInfo + user.userdogs[i].dogName + ", ";
    }
    rows += `
    <div class="row">
        <div class="col-md-12">
        ${generateUserRowHeadingProfileName(user)}
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
        ${generateUserRowHeadingPhoto(user)}
        </div>
        <div class="col-md-9">
        ${generateCurrentUserRowElement(
        'Email:', user.email != null ? user.email : 'info puudub',
        'Kasutajanimi:', user.username != null ? user.username : 'info puudub'
        )}
        ${generateCurrentUserRowElement(
        'Telefoni number:', user.phoneNumber > 0 ? formatNumber(user.phoneNumber) : 'info puudub',
        'Elukoht:', user.userlocation != null ? user.userlocation : 'info puudub'
        )}
    
        ${generateCurrentUserWideRowValueElement('Koer(ad):', dogInfo)}

        ${generateCurrentUserRowButtons(user)}
            `;
    document.getElementById("currentUserList").innerHTML = `
            <div class="row justify-content-center">
                <div class="col-lg-10">
            ${rows}
                </div>
            </div>
        </div>
        `;
    generateCurrentUserDogList(user);
}
function generateUserRowHeadingProfileName(user) {
    return `
        <div class="row">
            <div class="col-12" style="padding: 10px;">
                <strong style="font-size: 28px;">${user.ownerName}</strong>
            </div>
        </div>
    `;
}

function generateUserRowHeadingPhoto(user) {
    return `
        <div class="row">
            <div class="col-12" style="padding: 5px;">
                ${generateCurrentUserImageElement(user.userphoto)}
            </div>
        </div>
    `;
}


function generateCurrentUserRowElement(cell1Content, cell2Content, cell3Content, cell4Content) {
    return `
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    ${generateCurrentUserRowTitleCell(cell1Content)}
                    ${generateCurrentUserRowValueCell(cell2Content)}
                </div>
            </div>
            <div class="col-md-6">
                <div class="row">
                ${generateCurrentUserRowTitleCell(cell3Content)}
                ${generateCurrentUserRowValueCell(cell4Content)}
                </div>
            </div>
    </div>
    `;
}

function generateCurrentUserRowTitleCell(text) {
    return `
        <div class="col-sm-6" style="padding: 5px;">
            <div style="background: #EAEDED; border-top: 1px solid #CCD1D1; border-bottom: 1px solid #CCD1D1; margin: 2px; padding: 10px; text-align: center;">
                <strong>${text}</strong>
            </div>
        </div>
    `;
}

function generateCurrentUserRowValueCell(text) {
    return `
        <div class="col-sm-6" style="padding: 5px;">
            <div style="border-top: 1px solid #CCD1D1; border-bottom: 1px solid #CCD1D1; margin: 2px; padding: 10px; text-align: center;">
                ${text}
            </div>
        </div>
    `;
}

function generateCurrentUserWideRowValueElement(cell1Content, cell2Content) {
    return `
    <div class="row">
        <div class="col-md-3">
            <div class="row">
                ${generateCurrentUserWideRowTitleCell(cell1Content)}
            </div>
        </div>
        <div class="col-md-9">
            <div class="row">
                ${generateCurrentUserWideRowValueCell(cell2Content)}
            </div>
        </div>
    </div>
    `;
}

function generateCurrentUserWideRowValueCell(text) {
    return `
    <div class="col-sm-12" style="padding: 5px;">
        <div style="border-top: 1px solid #CCD1D1; border-bottom: 1px solid #CCD1D1; margin: 2px; padding: 10px; text-align: center;">
            ${text}
        </div>
    </div>
`;
}

function generateCurrentUserWideRowTitleCell(text) {
    return `
        <div class="col-sm-12" style="padding: 5px;">
            <div style="background: #EAEDED; border-top: 1px solid #CCD1D1; border-bottom: 1px solid #CCD1D1; margin: 2px; padding: 10px; text-align: center;">
                <strong>${text}</strong>
            </div>
        </div>
    `;
}

function generateCurrentUserRowButtons(user) {
    return `
        <div class="row justify-content-center">
            <div class="col-sm-3" style="padding: 10px;">
                <button class="btn btn-primary btn-block" onClick="openCurrentUserModalForUpdate(${user.id})">Muuda</button>
            </div>
            <div class="col-sm-3" style="padding: 10px;">
                <button class="btn btn-danger btn-block" onClick="removeUser(${user.id})">Kustuta</button>
            </div>
        </div>
        <div class="row">
            <div class="col-12" style="padding: 10px;">
                <hr/>
            </div>
        </div>
    `;
}

function generateCurrentUserImageElement(userphoto) {
    if (!isEmpty(userphoto)) {
        return `<img src="${userphoto}" width="250" class="rounded"/>`;
    } else {
        return "[PILT PUUDUB]";
    }
}

function openCurrentUserModal() {
    $("#currentUserModal").modal('show');
}

function closeCurrentUserModal() {
    $("#currentUserModal").modal('hide');
}

function clearError() {
    document.getElementById("errorPanel").innerHTML = "";
    document.getElementById("errorPanel").style.display = "none";
}

function showError(message) {
    document.getElementById("errorPanel").innerHTML = message;
    document.getElementById("errorPanel").style.display = "block";
}

function clearCurrentUserModal() {
    document.getElementById("editID").value = null;
    document.getElementById("editUsername").value = null;
    document.getElementById("editPassword").value = null;
    document.getElementById("editOwnerName").value = null;
    document.getElementById("editEmail").value = null;
    document.getElementById("editPhoneNumber").value = null;
    document.getElementById("editProfilephoto").value = null;
}

function fillCurrentUserModal(user) {
    document.getElementById("editID").value = user.id;
    document.getElementById("editUsername").value = user.username;
    document.getElementById("editPassword").value = user.password;
    document.getElementById("editOwnerName").value = user.ownerName;
    document.getElementById("editEmail").value = user.email;
    document.getElementById("editPhoneNumber").value = user.phoneNumber;
    document.getElementById("editProfilephoto").value = user.userphoto;
}

function fillUserModalLogoField(userphoto) {
    document.getElementById("userphoto").value = userphoto;
}