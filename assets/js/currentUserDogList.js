function generateCurrentUserDogList(user) {
    let rows = "";
    for (let i = 0; i < user.userdogs.length; i++) {
        let playGroundText = "";
        let locationText = "";
        for (let j = 0; j < user.userdogs[i].dogplayground.length; j++) {
            playGroundText = playGroundText + user.userdogs[i].dogplayground[j].playground + ", ";
        }
        for (let x = 0; x < user.userdogs[i].dogplayground.length; x++) {
            locationText = locationText + user.userdogs[i].dogplayground[x].location + ", ";
        }


        rows += `
            <div class="row">
                <div class="col-12">
                    ${generateUserDogRowHeadingUsername(user.userdogs[i])}
                 </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    ${generateUserDogRowHeadingPhoto(user.userdogs[i])}
                </div>
                <div class="col-md-8">
                    ${generateUserDogRowElement(
            'Koeranimi:', user.userdogs[i].dogName != null ? user.userdogs[i].dogName : 'info puudub',
            'Tõug:', user.userdogs[i].breedName != null ? user.userdogs[i].breedName : 'info puudub'
        )}
                    ${generateUserDogRowElement(
            'Sünnikuupäev:', user.userdogs[i].bornDate != null ? user.userdogs[i].bornDate : 'info puudub',
            'Vanus:', user.userdogs[i].age != null ? user.userdogs[i].age : 'info puudub'
        )}
                    ${generateUserDogRowWideElement('Elukoht:', locationText)}
                    ${generateUserDogRowWideElement('Mänguplatsid:', playGroundText)}
                    ${generateUserDogRowWideElement('Kirjeldus:', user.userdogs[i].description != null ? user.userdogs[i].description : 'info puudub')}

                    ${generateUserDogButtons(user.userdogs[i])}
                </div>
            </div>
            `;
    }

    document.getElementById("currentUserDogList").innerHTML = `
        <div class="row justify-content-center">
            <div class="col-lg-10">
                ${rows}
                <div class="row">
                    <div class="col-12" style="padding: 5px;">
                        <button class="btn btn-success btn-block" onClick="handleAddUserDogButtonClick()">Lisa koer</button>
                    </div>
                </div>
            </div>
        </div>
    `;
}
function generateUserDogRowHeadingUsername(user) {
    return `
        <div class="row" >
            <div class="col-md-12">
                <strong style="font-size: 28px;">${user.dogName}</strong>
            </div>
        </div>
    `;
}

function generateUserDogRowHeadingPhoto(user) {
    return `
        <div>
            ${generateCurrentUserDogImageElement(user.profilePhoto)}
        </div>
    `;
}

function generateUserDogRowElement(cell1Content, cell2Content, cell3Content, cell4Content) {
    return `
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    ${generateUserDogRowTitleCell(cell1Content)}
                    ${generateDogRowValueCell(cell2Content)}
                </div>
            </div>
            <div class="col-md-6">
                <div class="row">
                    ${generateUserDogRowTitleCell(cell3Content)}
                    ${generateDogRowValueCell(cell4Content)}
                </div>
            </div>
        </div>
    `;
}

function generateUserDogRowWideElement(cell1Content, cell2Content) {
    return `
        <div class="row">
            <div class="col-md-3 ">
                <div class="row">
                    ${generateUserDogRowWideTitleCell(cell1Content)}
                </div>
            </div>
            <div class="col-md-9 ">
                <div class="row">
                    ${generateUserDogRowWideTitleCell(cell2Content)}
            </div>
            </div>
        </div>
    `;
}

function generateUserDogRowTitleCell(text) {
    return `
        <div class="col-sm-6" style="padding: 5px;">
            <div style="background: #EAEDED; border-top: 1px solid #CCD1D1; border-bottom: 1px solid #CCD1D1; margin: 0px; padding: 10px; text-align: center;">
                <strong>${text}</strong>
            </div>
        </div>
    `;
}

function generateDogRowValueCell(text) {
    return `
        <div class="col-sm-6" style="padding: 5px;">
            <div style="border-top: 1px solid #CCD1D1; border-bottom: 1px solid #CCD1D1; margin: 2px; padding: 10px; text-align: center;">
                ${text}
            </div>
        </div>
    `;
}


function generateUserDogRowWideTitleCell(text) {
    return `
        <div class="col-sm-12" style="padding: 5px;">
            <div style="background: #EAEDED; border-top: 1px solid #CCD1D1; border-bottom: 1px solid #CCD1D1; margin: 0px; padding: 10px; text-align: center;">
                <strong>${text}</strong>
            </div>
        </div>
    `;
}

function generateUserDogButtons(dog) {
    return `
        <div class="row justify-content-center">
            <div class="col-sm-4" style="padding: 10px;">
                <button class="btn btn-primary btn-block" color: green onClick="openCurrentDogModalForUpdate(${dog.id})">Muuda</button>
            </div>
            <div class="col-sm-4" style="padding: 10px;">
                <button class="btn btn-danger btn-block" onClick="removeUserDog(${dog.id})">Kustuta</button>
            </div>
        </div>
        <div class="row">
            <div class="col-12" style="padding: 10px;">
                <hr/>
            </div>
        </div>
    `;
}

function generateCurrentUserDogImageElement(profilePhoto) {
    if (!isEmpty(profilePhoto)) {
        return `<img src="${profilePhoto}" width="370" class="rounded"/>`;
    } else {
        return "[PILT PUUDUB]";
    }
}

function openCurrentUserDogModal() {
    $("#currentUserDogModal").modal('show');
}

function closeCurrentUserDogModal() {
    $("#currentUserDogModal").modal('hide');
}

function clearCurrentUserDogModal() {
    document.getElementById("modifyID").value = null;
    document.getElementById("dogName").value = null;
    document.getElementById("breedName").value = null;
    document.getElementById("bornDate").value = null;
    document.getElementById("locationID").value = null;
    document.getElementById("profilePhoto").value = null;
}

function clearCurrentUserDogUpdateModal() {
    document.getElementById("updateID").value = null;
    document.getElementById("updateDogName").value = null;
    document.getElementById("updateBreedName").value = null;
    document.getElementById("updateBornDate").value = null;
    document.getElementById("updateLocationID").value = null;
    document.getElementById("updateDescription").value = null;
    document.getElementById("updateProfilePhoto").value = null;
}

function fillCurrentUserDogUpdateModal(dog) {
    if (dog != null) {
        document.getElementById("updateID").value = dog.id;
        document.getElementById("updateDogName").value = dog.dogName;
        document.getElementById("updateBreedName").value = dog.breedName;
        document.getElementById("updateBreedID").value = dog.breedId;
        document.getElementById("updateBornDate").value = dog.bornDate;
        document.getElementById("updateLocationID").value = dog.locationId;
        document.getElementById("updateDescription").value = dog.description;
        document.getElementById("updateProfilePhoto").value = dog.profilePhoto;
    }
}

function fillUserDogModalLogoField(profilephoto) {
    document.getElementById("profilephoto").value = profilephoto;
}
