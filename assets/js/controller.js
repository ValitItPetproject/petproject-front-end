
let users = [];

function handleSearchkeyOwner() {
    let searchKey = document.getElementById("searchKeyOwener").value;
    let filteredUsers = searchUsers(searchKey);
    generateUserList(filteredUsers);
}

function searchUsers(searchKeyword) {
    let resultingUsers = [];

    for (let i = 0; i < users.length; i++) {
        if (users[i].ownerName.toLowerCase().search(searchKeyword.toLowerCase()) != -1) {
            resultingUsers.push(highlightSearchMatchUser(users[i], searchKeyword));
        }
    }

    return resultingUsers;
}

function highlightSearchMatchUser(user, searchKey) {
    let highlightedUser = {...user}; // Teeme koopia algsest ettevõttest, ära muuda originaali!!!
    const re = new RegExp(searchKey, "gi");
    highlightedUser.ownerName = highlightedUser.ownerName.replace(re, `<span style="color: #17a2b8; font-weight: bolder; background: #E5E8E8;">${searchKey}</span>`);
    return highlightedUser;
}

function loadUsers() {
    fetchUsers().then(userList => {
        users = userList;
        generateUserList(userList);
    });
}

function loadCurrentUserList() {
    fetchCurrentUser().then(generateCurrentUserList);
}

function loadPlaygrounds() {
    fetchPlaygrounds().then(generatePlaygroundList);
}

function loadDogList() {
    fetchDogList().then(generateDogList);
}

function openCurrentUserDogModal(id) {
    clearError();
    fetchCurrentUser(id).then(user => {
        clearCurrentUserDogModal();
        fillCurrentUserDogModal(user);
    });
}

function openCurrentUserModalForUpdate(id) {
    clearError();
    fetchCurrentUser(id).then(user => {
        openCurrentUserModal();
        clearCurrentUserModal();
        fillCurrentUserModal(user);
    });
}

function openCurrentDogModalForUpdate(id) {
    clearError();
    fetchCurrentUser().then(user => {
        openCurrentUserDogUpdateModal();
        clearCurrentUserDogUpdateModal();
        let dog = user.userdogs.find(d => d.id == id);
        fillCurrentUserDogUpdateModal(dog);
    })
}

function openCurrentUserModalForInsert() {
    openCurrentUserModal();
    clearError();
    clearCurrentUserModal();
}


function saveFile(event) {
    event.preventDefault();
    let fileInput = getFileInput();
    uploadFile(fileInput.files[0]).then(response => fillUserModalLogoField(response.url));
}

function saveUser() {
    let user = getUserFromModal();
    if (validateUserModal(user)) {
        postUser(user)
            .then(() => {
                closeUserModal();
            }
        );
    }
}

function removeUser(id) {
    if (confirm('Soovid kasutajat kustutada?')) {
        deleteUser(id).then(loadUsers)
    }
}

function removeUserDog(id) {
    if (confirm('Soovid kasutajat kustutada?')) {
        deleteUserDog(id).then(loadUsers)
    }
}

function loginUser() {
    let credentials = getCredentialsFromLoginContainer();
    if (validateCredentials(credentials)) {
        login(credentials).then(session => {
            storeAuthentication(session);
            generateTopMenu();
            loadUsers();
        })
    }
}

function logoutUser() {
    clearAuthentication();
    generateTopMenu();
    showLoginContainer();
}

// Loo kasutaja 

function handleAddUserButtonClick() {
    $("#userModal").modal("show");
    document.getElementById("id").value = null;
    document.getElementById("username").value = null;
    document.getElementById("password").value = null;
    document.getElementById("ownerName").value = null;
    document.getElementById("email").value = null;
    document.getElementById("phoneNumber").value = null;
}

function handleAddUserDogButtonClick() {
    $("#currentUserDogModal").modal("show");
    document.getElementById("modifyID").value = null;
    document.getElementById("dogName").value = null;
    document.getElementById("breedName").value = null;
    document.getElementById("bornDate").value = null;
    document.getElementById("locationID").value = null;
    document.getElementById("profilePhoto").value = null;
}

function handleUpdateUserDogButtonClick() {
    $("#currentUserDogUpdateModal").modal("show");
    document.getElementById("updateID").value = null;
    document.getElementById("updateDogName").value = null;
    document.getElementById("updateBreedName").value = null;
    document.getElementById("updateBreedID").value = null;
    document.getElementById("updateBornDate").value = null;
    document.getElementById("updateLocationID").value = null;
    document.getElementById("updateDescription").value = null;
    document.getElementById("updateProfilePhoto").value = null;
}

function handleSaveCurrentUserDog() {
    if (isDogUpdateFormValid() === false) {
        return;
    }
    if (document.getElementById("id").value > 0) {
        handleEditCurrentUserDog();
    } else {
        handleAddCurrentUserDog();
    }
}

function handleUpdateCurrentUserDog() {
    if (isDogUpdateFormValid() === false) {
        return;
    }
    if (document.getElementById("updateID").value > 0) {
        handleUpdateDog();
    }
}

function handleUpdateCurrentUser() {
    if (isUserFormValid() === false) {
        return;
    }
    if (document.getElementById("id").value > 0) {
        handleEditCurrentUser();
    } else {
        handleUpdateUser();
        loadCurrentUserList();
    }
}

function handleUpdateUser() {
    let user = {
        id: document.getElementById("editID").value,
        username: document.getElementById("editUsername").value,
        password: document.getElementById("editPassword").value,
        ownerName: document.getElementById("editOwnerName").value,
        email: document.getElementById("editEmail").value,
        phoneNumber: document.getElementById("editPhoneNumber").value,
        userphoto: document.getElementById("editProfilephoto").value,
    };
    putUser(user).then(
        function () {
            $("#currentUserModal").modal("hide");
            closeCurrentUserModal();
        }
    );
}

function handleAddCurrentUserDog() {
    let user = {
        id: document.getElementById("modifyID").value,
        dogName: document.getElementById("dogName").value,
        breedId: document.getElementById("breedName").value,
        bornDate: document.getElementById("bornDate").value,
        locationId: document.getElementById("locationId").value,
        profilePhoto: document.getElementById("profilePhoto").value,
    };
    postUserDog(user).then(
        function () {
            $("#currentUserDogModal").modal("hide");
        }
    );
}

function handleUpdateDog() {
    let dog = {
        id: document.getElementById("updateID").value,
        dogName: document.getElementById("updateDogName").value,
        breedName: document.getElementById("updateBreedName").value,
        breedId: document.getElementById("updateBreedID").value,
        bornDate: document.getElementById("updateBornDate").value,
        locationId: document.getElementById("updateLocationID").value,
        description: document.getElementById("updateDescription").value,
        profilePhoto: document.getElementById("updateProfilePhoto").value,
    };
    putDog(dog).then(
        function () {
            $("#currentUserDogUpdateModal").modal("hide");
            closeCurrentUserDogUpdateModal();
        }
    );
}

function openCurrentUserDogUpdateModal() {
    $("#currentUserDogUpdateModal").modal('show');
}

function closeCurrentUserDogUpdateModal() {
    $("#currentUserDogUpdateModal").modal('hide');
}

function handleSave() {
    if (isFormValid() === false) {
        return;
    }
    if (document.getElementById("id").value > 0) {
        handleEdit();
    } else {
        handleAdd();
    }
}

function handleAdd() {
    let user = {
        username: document.getElementById("registerUsername").value,
        password: document.getElementById("registerPassword").value,
        ownerName: document.getElementById("ownerName").value,
        email: document.getElementById("email").value,
        phoneNumber: document.getElementById("phoneNumber").value,
    };
    postUser(user).then(
        function () {
            $("#userModal").modal("hide");
        }
    );
}

function isFormValid() {
    let username = document.getElementById("registerUsername").value;
    let password = document.getElementById("registerPassword").value;
    if (username === null || username.length < 1) {
        document.getElementById("errorMessage").innerText = "Kasutaja nimi on puudu";
        document.getElementById("errorMessage").style.display = "block";
        return false;
    }
    if (password === null || password.length < 1) {
        document.getElementById("errorMessage").innerText = "Salasõna on puudu"
        document.getElementById("errorMessage").style.display = "block";
        return false;
    }
    document.getElementById("errorMessage").style.display = "none";
}

function isUserFormValid() {
    let username = document.getElementById("editUsername").value;
    let password = document.getElementById("editPassword").value;
    if (username === null || username.length < 1) {
        document.getElementById("errorMessage").innerText = "Kasutaja nimi on puudu";
        document.getElementById("errorMessage").style.display = "block";
        return false;
    }
    if (password === null || password.length < 1) {
        document.getElementById("errorMessage").innerText = "Kasutajal puudub parool"
        document.getElementById("errorMessage").style.display = "block";
        return false;
    }
    document.getElementById("errorMessage").style.display = "none";
}

function isDogUpdateFormValid() {
    
    let dogName = document.getElementById("updateDogName").value;
    if (dogName === null || dogName.length < 1) {
        console.log('OK');
        document.getElementById("errorMessage").innerText = "Koera nimi on puudu";
        document.getElementById("errorMessage").style.display = "block";
        return false;
    }
    document.getElementById("errorMessage").style.display = "none";
    return true;
}