
function generateUserList(users) {
    let rows = "";

    for (let i = 0; i < users.length; i++) {
        rows += `
        <div class="row">
            <div class="col-md-12">
                ${generateUserRowHeadingOwnerName(users[i])}
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                ${generateUserRowHeadingUserphoto(users[i])}
            </div>
            <div class="col-md-9">
                ${generateUserRowElement('Omaniku nimi:', users[i].ownerName != null ? users[i].ownerName : 'info puudub')}
                ${generateUserRowElement('Telefoni number:', users[i].phoneNumber > 0 ? formatNumber(users[i].phoneNumber) : 'info puudub')}
                ${generateUserRowElement('Elukoht:', users[i].userlocation != null ? users[i].userlocation : 'info puudub')}
                ${generateUserRowElement('Email:', users[i].email != null ? users[i].email : 'info puudub')}
            </div>
        </div>
        `;
    }
    
    document.getElementById("userList").innerHTML = `
        <div class="row justify-content-center">
            <div class="col-lg-10">
        ${rows}
                </div>
            </div>
    `;
}

function generateUserRowHeadingOwnerName (users) {
    return `
        <div class="row" id = "HeadingRow">
            <div class="col-md-12" style="padding: 10px;">
                <strong style="font-size: 28px;">${users.ownerName}</strong>
            </div>
        </div>
    `;
}

function generateUserRowHeadingUserphoto (users) {
    return `
    <div class="row">
        <div class="col-md-12" style="padding: 10px;">
            ${generateUserListImageElement(users.userphoto)}
        </div>
    </div>
`;
}

function generateUserListImageElement(userphoto) {
    if (!isEmpty(userphoto)) {
        return `<img src="${userphoto}" width="250" class="rounded"/>`;
    } else {
        return "[PILT PUUDUB]";
    }
}


function generateUserRowElement(cell1Content, cell2Content) {
    return `
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    ${generateUserRowTitleCell(cell1Content)}
                    ${generateUserRowTitleCell(cell2Content)}
                </div>
            </div>
        </div>
    `;
}

function generateUserRowTitleCell(text) {
    return `
        <div class="col-sm-6" style="padding: 5px;">
            <div style="background: #EAEDED; border-top: 1px solid #CCD1D1; border-bottom: 1px solid #CCD1D1; margin: 2px; padding: 10px; text-align: center;">
                <strong>${text}</strong>
            </div>
        </div>
    `;
}