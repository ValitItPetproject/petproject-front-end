
function fetchUsers() {
    return fetch(
        `${API_URL}/users`,
        {
            method: 'GET',
            headers: {
                'Authorization': `Bearer ${getToken()}`
            }
        }
    )
    .then(checkResponse)
    .then(users => users.json());
}

function fetchUser(id) {
    return fetch(
        `${API_URL}/users/${id}`,
        {
            method: 'GET',
            headers: {
                'Authorization': `Bearer ${getToken()}`
            }
        }
    )
    .then(checkResponse).then(users => users.json());
}

function fetchPlaygrounds() {
    return fetch(
        `${API_URL}/dogs/playgrounds`,
        {
            method: 'GET',
            headers: {
                'Authorization': `Bearer ${getToken()}`
            }
        }
    )
    .then(checkResponse).then(dogs => dogs.json());
}

function fetchDogList() {
    return fetch(
        `${API_URL}/dogs`,
        {
            method: 'GET',
            headers: {
                'Authorization': `Bearer ${getToken()}`
            }
        }
    )
    .then(checkResponse).then(users => users.json());
}

function fetchCurrentUser() {
    return fetch(
        `${API_URL}/users/current`,
        {
            method: 'GET',
            headers: {
                'Authorization': `Bearer ${getToken()}`
            }
        }
    )
    .then(checkResponse).then(users => users.json());
}

function postUser(users) {
    return fetch(
        `${API_URL}/users/register`, 
        {
            method: 'POST',
            headers: {
                "Content-Type": "application/json"
            }, 
            body: JSON.stringify(users)
        }
    );
}

function putUser(user) {
    return fetch(
        `${API_URL}/users`, 
        {
            method: 'PUT',
            headers: {
                "Content-Type": "application/json",
                'Authorization': `Bearer ${getToken()}`
            }, 
            body: JSON.stringify(user)
        }
    );
}

function putDog(dog) {
    return fetch(
        `${API_URL}/dogs`, 
        {
            method: 'PUT',
            headers: {
                "Content-Type": "application/json",
                'Authorization': `Bearer ${getToken()}`
            }, 
            body: JSON.stringify(dog)
        }
    );
}

function postUserDog(dog) {
    return fetch(
        `${API_URL}/dogs`, 
        {
            method: 'POST',
            headers: {
                "Content-Type": "application/json",
                'Authorization': `Bearer ${getToken()}`
            }, 
            body: JSON.stringify(dog)
        }
    );
}

function uploadFile(file) {
    let formData = new FormData();
    formData.append("file", file);

    return fetch(
            `${API_URL}/files/upload`, 
            {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${getToken()}`
                },
                body: formData
            }
    )
    .then(checkResponse).then(response => response.json());
}

function deleteUser(id) {
    return fetch(
        `${API_URL}/users/${id}`,
        {
            method: 'DELETE',
            headers: {
                'Authorization': `Bearer ${getToken()}`
            }
        }
    )
    .then(checkResponse);
}

function deleteUserDog(id) {
    return fetch(
        `${API_URL}/dogs/${id}`,
        {
            method: 'DELETE',
            headers: {
                'Authorization': `Bearer ${getToken()}`
            }
        }
    )
    .then(checkResponse);
}

function login(credentials) {
    return fetch(
        `${API_URL}/users/login`,
        {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(credentials)
        }
    )
    .then(checkResponse)
    .then(session => session.json());
}

function checkResponse(response) {
    if (!response.ok) {
        clearAuthentication();
        showLoginContainer();
        // closeUserModal();
        generateTopMenu();
        throw new Error(response.status);
    }
    showMainContainer();
    return response;
}
