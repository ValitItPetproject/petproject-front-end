let dogs = [];

/* Otsing soo järgi */

function handleSearchkeygender() {
    let filteredDogs = searchFemaledog();
    generateDogList(filteredDogs);
}

function searchFemaledog (){
    let filteredDogs = [];
    for (let i = 0; i < dogs.length; i++) {
        if (dogs[i].female) {
            filteredDogs.push(dogs[i]);
        }
    }

    return filteredDogs;       
}

function handleSearchkeygenderMale() {
    let filteredDogs = searchMaledog();
    generateDogList(filteredDogs);
}

function searchMaledog (){
    let filteredDogs = [];
    for (let i = 0; i < dogs.length; i++) {
        if (!dogs[i].female) {
            filteredDogs.push(dogs[i]);
        }
    }

    return filteredDogs;       
}

/* Otsing suuruse järgi */
function handleSearchkeySizeBig() {
    var inputsize = "suur"
    let sizeDogs = divideDogBySize(inputsize);
    generateDogList(sizeDogs);
}

function handleSearchkeySizeMedium() {
    var inputsize = "keskmine"
    let sizeDogs = divideDogBySize(inputsize);
    generateDogList(sizeDogs);
}

function handleSearchkeySizeSmall() {
    var inputsize = "väike"
    let sizeDogs = divideDogBySize(inputsize);
    generateDogList(sizeDogs);
}


function divideDogBySize (size){
    let sizeFilteredDogs = [];
    for (let i = 0; i < dogs.length; i++) {
        if (dogs[i].size == size) {
            sizeFilteredDogs.push(dogs[i]);
        }
    }

    return sizeFilteredDogs;       
}




/* Otsing tõu järgi */

function handleSearchkeyBreed() {
    let searchKey = document.getElementById("searchdogbreed").value;
    let filteredDogs = searchDogsByBreed(searchKey);
    generateDogList(filteredDogs);
}

function highlightSearchMatchBreed(dog, searchKey) {
    let highlightedDog = {...dog}; // Teeme koopia algsest koerast, ära muuda originaali!!!
    const re = new RegExp(searchKey, "gi");
    highlightedDog.breedName = highlightedDog.breedName.replace(re, `<span style="color: #17a2b8; font-weight: bolder; background: #E5E8E8;">${searchKey}</span>`);
    return highlightedDog;
}

function searchDogsByBreed(searchKeyword) {
    let resultingDogs = [];

    for (let i = 0; i < dogs.length; i++) {
        if (dogs[i].breedName.toLowerCase().search(searchKeyword.toLowerCase()) != -1) {
            resultingDogs.push(highlightSearchMatchBreed(dogs[i], searchKeyword));
        }
    }

    return resultingDogs;
}

/* Otsing koha järgi */

function handleSearchkeyLocation() {
    let searchKey = document.getElementById("searchdoglocation").value;
    let filteredDogs = searchDogsByLocation(searchKey);
    generateDogList(filteredDogs);
}

function highlightSearchMatchLocation(dog, searchKey) {
    let highlightedDog = {...dog}; // Teeme koopia algsest koerast, ära muuda originaali!!!
    const re = new RegExp(searchKey, "gi");
    highlightedDog.locationInfo = highlightedDog.locationInfo.replace(re, `<span style="color: #17a2b8; font-weight: bolder; background: #E5E8E8;">${searchKey}</span>`);
    return highlightedDog;
}

function searchDogsByLocation(searchKeyword) {
    let resultingDogs = [];

    for (let i = 0; i < dogs.length; i++) {
        if (locationInfo.toLowerCase().search(searchKeyword.toLowerCase()) != -1) {
            resultingDogs.push(highlightSearchMatchLocation(dogs[i], searchKeyword));
        }
    }

    return resultingDogs;
}

function loadDogs() {
    fetchDogList().then(dogList => {
        dogs = dogList;
        generateDogList(dogList);
    });
}
