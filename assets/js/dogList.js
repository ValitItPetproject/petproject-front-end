function generateDogList(dog) {
    let rows = "";
    
    for (let i = 0; i < dog.length; i++) {
        let playGroundInfo = "";
        let locationInfo = "";
        for (let y = 0; y < dog[i].dogplayground.length; y++) {
            playGroundInfo = playGroundInfo + dog[i].dogplayground[y].playground + ", ";
        }
        for (let x = 0; x < dog[i].dogplayground.length; x++) {
            locationInfo = locationInfo + dog[i].dogplayground[x].location + ", ";
        }
        rows += `
        <div class="row" id = "HeadingRow">
            <div class="col-2" id = "dogsHeadingCol1">
            ${generateDogListRowHeadingDogName(dog[i])}
            </div>
            <div class="col-1" id = "dogsHeadingCol2">
            ${generateSizeIcon(dog[i])}
            </div>
            <div class="col-9" id = "dogsHeadingCol3">
            ${generateGenderIcon(dog[i])}
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
            ${generateDogListRowHeadingDogPhoto(dog[i])}
            </div>
            <div class="col-md-8">
            ${generateDogListRowElement(
            'Koera nimi:', dog[i].dogName != null ? dog[i].dogName : 'info puudub',
            'Tõug:', dog[i].breedName != null ? dog[i].breedName : 'info puudub'
            )}
            ${generateDogListRowElement(
            'Sünnikuupäev:', dog[i].bornDate != null ? dog[i].bornDate : 'info puudub',
            'Vanus:', dog[i].age != null ? dog[i].age : 'info puudub'
            )}

            ${generateDogListRowWideElement('Elukoht:', locationInfo)}
            ${generateDogListRowWideElement('Mänguplatsid:', playGroundInfo)}
            ${generateDogListRowWideElement('Kirjeldus:', dog[i].description != null ? dog[i].description : 'info puudub')}
            
            </div>
        </div>
        `;
    }
    document.getElementById("dogList").innerHTML = `
        <div class="row justify-content-center">
            <div class="col-lg-10">
                ${rows}
            </div>
        </div>
    `;
}
function generateDogListRowHeadingDogName(dog) {
    return `
        <div class="row">
                <strong style="font-size: 28px;">${dog.dogName}</strong>
        </div>
    `;
    
}

function generateGenderIcon(dog) {
    if (dog.female){
        return `
        <div class="row">
            <img class="image1" src="assets/images/femaleico.png" alt="image">
        </div>
    `;
    }
    else
    return `
    <div class="row">
        <img class="image1" src="assets/images/maleico.png" alt="image">
    </div>
`;
}

function generateSizeIcon(dog) {
    if (dog.size == "väike"){
        return `
        <div class="row">
            <img class="image1" src="assets/images/taks.png" alt="image">
        </div>
    `;
    }
    else if(dog.size == "keskmine" ){
    return `
    <div class="row">
        <img class="image1" src="assets/images/keskmine.png" alt="image">
    </div>
    `;
    }
    else
    return `
    <div class="row">
        <img class="image1" src="assets/images/suur.png" alt="image">
    </div>
`;
}


function generateDogListRowHeadingDogPhoto(dog) {
    return `
        <div>
            ${generateDogListImageElement(dog.profilePhoto)}
        </div>
    `;
}

function generateDogListImageElement(profilePhoto) {
    if (!isEmpty(profilePhoto)) {
        return `<img src="${profilePhoto}" width="370" class="rounded"/>`;
    } else {
        return "[PILT PUUDUB]";
    }
}

function generateDogListRowElement(cell1Content, cell2Content, cell3Content, cell4Content) {
    return `
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    ${generateDogListRowTitleCell(cell1Content)}
                    ${generateDogListRowValueCell(cell2Content)}
                </div>
            </div>
            <div class="col-md-6">
                <div class="row">
                    ${generateDogListRowTitleCell(cell3Content)}
                    ${generateDogListRowValueCell(cell4Content)}
                </div>
            </div>
        </div>
    `;
}

function generateDogListRowWideElement(cell1Content, cell2Content) {
    return `
        <div class="row">
            <div class="col-md-3 ">
                <div class="row">
                    ${generateDogListRowWideTitleCell(cell1Content)}
                </div>
            </div>
            <div class="col-md-9 ">
                <div class="row">
                    ${generateDogListRowWideValueCell(cell2Content)}
            </div>
            </div>
        </div>
    `;
}

function generateDogListRowTitleCell(text) {
    return `
        <div class="col-sm-6" style="padding: 5px;">
            <div style="background: #EAEDED; border-top: 1px solid #CCD1D1; border-bottom: 1px solid #CCD1D1; margin: 0px; padding: 10px; text-align: center;">
                <strong>${text}</strong>
            </div>
        </div>
    `;
}

function generateDogListRowValueCell(text) {
    return `
        <div class="col-sm-6" style="padding: 5px;">
            <div style="border-top: 1px solid #CCD1D1; border-bottom: 1px solid #CCD1D1; margin: 2px; padding: 10px; text-align: center;">
                ${text}
            </div>
        </div>
    `;
}

function generateDogListRowWideTitleCell(text) {
    return `
        <div class="col-sm-12" style="padding: 5px;">
            <div style="background: #EAEDED; border-top: 1px solid #CCD1D1; border-bottom: 1px solid #CCD1D1; margin: 0px; padding: 10px; text-align: center;">
                <strong>${text}</strong>
            </div>
        </div>
    `;
}

function generateDogListRowWideValueCell(text) {
    return `
    <div class="col-sm-12" style="padding: 5px;">
        <div style="border-top: 1px solid #CCD1D1; border-bottom: 1px solid #CCD1D1; margin: 2px; padding: 10px; text-align: center;">
            ${text}
        </div>
    </div>
`;
}