
// Validation functions

function validateCompanyModal(company) {
    if (isEmpty(company.name)) {
        showError("Nimi on kohustuslik!")
        return false;
    }
    if(!isEmpty(company.employees) && !isInteger(company.employees)) {
        showError("Töötajate arv peab olema täisarv!");
        return false;
    }
    if(!isEmpty(company.revenue) && !isDecimal(company.revenue)) {
        showError("Müügitulu peab olema number!");
        return false;
    }
    if(!isEmpty(company.netIncome) && !isDecimal(company.netIncome)) {
        showError("Netotulu peab olema number!");
        return false;
    }
    if(!isEmpty(company.securities) && !isInteger(company.securities)) {
        showError("Aktsiate arv peab olema täisarv!");
        return false;
    }
    if (!isEmpty(company.securityPrice) && !isDecimal(company.securityPrice)) {
        showError("Aktsiahind peab olema number!");
        return false;
    }
    if (!isEmpty(company.dividends) && !isDecimal(company.dividends)) {
        showError("Dididend peab olema number!");
        return false;
    }
    return true;
}

function validateCredentials(credentials) {
    return !isEmpty(credentials.username) && !isEmpty(credentials.password);
}

function isInteger(text) {
    if (text == null) {
        return false;
    }
    let regex = /^[\-]?\d+$/;
    return regex.test(text);
}

function isDecimal(text) {
    if (text == null) {
        return false;
    }
    let regex = /^[\-]?\d+(\.\d+)?$/;
    return regex.test(text);
}

function isEmpty(text) {
    return (!text || 0 === text.length);
}

// DOM retrieval functions

function getFileInput() {
    return document.getElementById("file");
}

function getCompanyFromModal() {
    return {
        "id": document.getElementById("id").value, 
        "name": document.getElementById("name").value,
        "logo": document.getElementById("logo").value,
        "established": document.getElementById("established").value,
        "employees": document.getElementById("employees").value,
        "revenue": document.getElementById("revenue").value,
        "netIncome": document.getElementById("netIncome").value,
        "securities": document.getElementById("securities").value,
        "securityPrice": document.getElementById("securityPrice").value,
        "dividends": document.getElementById("dividends").value
    };
}

function getCredentialsFromLoginContainer() {
    return {
        username: document.getElementById("username").value,
        password: document.getElementById("password").value
    };
}

// Financial functions

function formatNumber(num) {
    if (!isDecimal(num)) {
        return 0;
    }
    num = Math.round(num * 100) / 100;
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
}

// Chart Functions

function generateRandomColor() {
    let r = Math.floor(Math.random() * 255);
    let g = Math.floor(Math.random() * 255);
    let b = Math.floor(Math.random() * 255);
    return `rgb(${r},${g},${b})`;
}

function composeChartDataset(companies) {
    if (companies != null && companies.length > 0) {
        let data = companies.map(company => Math.round(company.marketCapitalization));
        let backgroundColors = companies.map(generateRandomColor);
        return [{
            data: data,
            backgroundColor: backgroundColors,
        }];
    }
    return [];
}

function composeChartLabels(companies) {
    return companies != null && companies.length > 0 ? companies.map(company => company.name) : [];
}

function composeChartData(companies) {
    return {
        datasets: composeChartDataset(companies),
        labels: composeChartLabels(companies)
    };
}
