
function generateTopMenu() {
    let logoutLink = "";
    if (!isEmpty(getUsername())) {
        logoutLink = `${getUsername()} | <a href="javascript:logoutUser()" style="color: black; font-weight: bold;">logi välja</a>`;
    }

    document.getElementById("topMenuContainer").innerHTML = `
        <div style="padding: 20px; color: black; font-style: italic; border-radius: 15px; background:#FAFAF2;">
            <div class="row">
                <div class="col-6">
                    <strong>Pety!</strong>
                </div>
                <div class="col-6" style="text-align: right; font-style: normal;">
                    ${logoutLink}
                </div>
            </div>
        </div>
    `;
}

function showLoginContainer() {
    document.getElementById("loginContainer").style.display = "block";
    document.getElementById("mainContainer").style.display = "none";
}

function showMainContainer() {
    document.getElementById("loginContainer").style.display = "none";
    document.getElementById("mainContainer").style.display = "block";
}

function showUserList() {
    document.getElementById("userList").style.display = "block";
    document.getElementById("currentUserList").style.display = "none";
    document.getElementById("currentUserDogList").style.display = "none";
    document.getElementById("playgroundList").style.display = "none";
    document.getElementById("dogList").style.display = "none";
    document.getElementById("dogSearchbox").style.display ="none";
    document.getElementById("ownerSearchbox").style.display ="block";
    loadUsers();
}

function showCurrentUserList() {
    document.getElementById("userList").style.display = "none";
    document.getElementById("currentUserList").style.display = "block";
    document.getElementById("currentUserDogList").style.display = "block";
    document.getElementById("playgroundList").style.display = "none";
    document.getElementById("dogList").style.display = "none";
    document.getElementById("dogSearchbox").style.display ="none";
    document.getElementById("ownerSearchbox").style.display ="none";
    loadCurrentUserList();
}

function showPlaygrounds() {
    document.getElementById("userList").style.display = "none";
    document.getElementById("currentUserList").style.display = "none";
    document.getElementById("currentUserDogList").style.display = "none";
    document.getElementById("playgroundList").style.display = "block";
    document.getElementById("dogList").style.display = "none";
    document.getElementById("dogSearchbox").style.display ="none";
    document.getElementById("ownerSearchbox").style.display ="none";
    loadPlaygrounds();
}

function showDogList() {
    document.getElementById("userList").style.display = "none";
    document.getElementById("currentUserList").style.display = "none";
    document.getElementById("currentUserDogList").style.display = "none";
    document.getElementById("playgroundList").style.display = "none";
    document.getElementById("dogList").style.display = "block";
    document.getElementById("dogSearchbox").style.display ="block";
    document.getElementById("ownerSearchbox").style.display ="none";
    loadDogList();
    loadDogs();
}
